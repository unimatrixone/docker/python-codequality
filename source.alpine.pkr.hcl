

source "docker" "latest" {
  image   = "python:alpine"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}",
    "ENTRYPOINT [\"python\"]",
    "ENV PYTHON_VERSION 3",
  ]
}


source "docker" "alpine" {
  image   = "python:alpine"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}",
    "ENV PYTHON_VERSION 3",
    "ENTRYPOINT [\"python\"]",
  ]
}


source "docker" "gitlab" {
  image   = "python:3.8-slim-buster"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "WORKDIR ${local.runtime_home}",
    "ENTRYPOINT [\"\"]",
    "ENV PYTHON_VERSION 3",
    "CMD []",
  ]
}

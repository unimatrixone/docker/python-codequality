

build {
  sources = [
    "source.docker.alpine",
    "source.docker.latest",
    "source.docker.gitlab",
  ]

  provisioner "file" {
    source = "files/SHA256SUMS"
    destination = "/tmp/SHA256SUMS"
  }

  provisioner "file" {
    source = "requirements.txt"
    destination = "/tmp/requirements.txt"
  }

  provisioner "shell" {
    inline = [
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "apk add --no-cache curl make git yaml-dev"
    ]
    only = ["docker.alpine", "docker.latest"]
  }

  provisioner "shell" {
    inline = [
      "apt update -y && apt install -y curl make git libyaml-dev"
    ]
    only = ["docker.gitlab"]
  }

  provisioner "shell" {
    inline = [
        "cd /tmp",
        "pip install -r requirements.txt"
      ]
  }

  provisioner "shell" {
    inline = [
      "rm -rf /tmp/*",
    ]
  }

  provisioner "shell" {
    inline = [
      "cat /etc/apk/repositories",
      "apk info -v | sort",
      "rm -rf /var/lib/apk/* /var/cache/apk/* /etc/apk/cache/*",
    ]
    only = ["docker.alpine", "docker.latest"]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        var.image_version,
      ]
      only        = ["docker.alpine"]
    }

    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        join("-", [var.image_version, "gitlab"])
      ]
      only        = ["docker.gitlab"]
    }

    # It is assumed here that when -only=latest is specified, the user does
    # not override the image_version variable.
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        regex_replace(var.image_version, "\\.[\\d]+$", ""),
        "latest",
      ]
      only        = ["docker.latest"]
    }

    post-processor "docker-push" {}
  }
}

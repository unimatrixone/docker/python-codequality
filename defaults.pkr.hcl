variable "docker_repository" { default=null }
variable "image_version" { default = "1.3.2" }


locals {
  docker_repository = "registry.gitlab.com/unimatrixone/docker/codequality/python"
  maintainer = "oss@unimatrixone.io"
}
